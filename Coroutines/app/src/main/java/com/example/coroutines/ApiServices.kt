package com.example.coroutines

import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query
import java.nio.channels.DatagramChannel

interface ApiServices {
    companion object{
        var BASE_URL = "https://ringdev.alo.vn"
        fun create() : ApiServices {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(ApiServices::class.java)
        }
    }
    @GET("/api/search/ring/name")
    suspend fun searchName(
        @Header("X-NetworkType") X_NetworkType: String,
        @Header("X-DeviceModel") X_DeviceModel : String,
        @Header("X-DeviceMemory") X_DeviceMemory: Float,
        @Header("X-AppVersion") X_AppVersion: String,
        @Header("language") language: String,
        @Header("country") country: String,
        @Query("name") name: String,
        @Query("pageNumber") pageNumber: Int
    ) : Response<Data>
}