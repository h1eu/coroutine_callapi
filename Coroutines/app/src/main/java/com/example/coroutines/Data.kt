package com.example.coroutines


import com.google.gson.annotations.SerializedName

data class Data(
    @SerializedName("data")
    var `data`: Data,
    @SerializedName("status")
    var status: Status
) {
    data class Data(
        @SerializedName("currentPage")
        var currentPage: Int,
        @SerializedName("data")
        var `data`: List<Data>,
        @SerializedName("nextPage")
        var nextPage: Int,
        @SerializedName("pageSize")
        var pageSize: Int
    ) {
        data class Data(
            @SerializedName("avgTrend")
            var avgTrend: Any,
            @SerializedName("category")
            var category: String,
            @SerializedName("displayHashtag")
            var displayHashtag: String,
            @SerializedName("down")
            var down: Any,
            @SerializedName("duration")
            var duration: Int,
            @SerializedName("endColorCode")
            var endColorCode: String,
            @SerializedName("firstHashtag")
            var firstHashtag: Any,
            @SerializedName("hashTag")
            var hashTag: String,
            @SerializedName("id")
            var id: String,
            @SerializedName("image")
            var image: String,
            @SerializedName("name")
            var name: String,
            @SerializedName("screenRatio")
            var screenRatio: Any,
            @SerializedName("secondHashtag")
            var secondHashtag: Any,
            @SerializedName("startColorCode")
            var startColorCode: String,
            @SerializedName("thirdHashtag")
            var thirdHashtag: Any,
            @SerializedName("type")
            var type: String,
            @SerializedName("url")
            var url: String
        )
    }

    data class Status(
        @SerializedName("message")
        var message: String,
        @SerializedName("statusCode")
        var statusCode: Int
    )
}