package com.example.coroutines

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import com.bumptech.glide.Glide
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    var BASE_URL = "https://ringdev.alo.vn/"
    var linkUrl = arrayListOf<String>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var img1 = findViewById<ImageView>(R.id.img1)
        var img2 = findViewById<ImageView>(R.id.img2)
        var img3 = findViewById<ImageView>(R.id.img3)
        var img4 = findViewById<ImageView>(R.id.img4)
        var img5 = findViewById<ImageView>(R.id.img5)
        var img6 = findViewById<ImageView>(R.id.img6)
        var img7 = findViewById<ImageView>(R.id.img7)
        var img8 = findViewById<ImageView>(R.id.img8)
        var img9 = findViewById<ImageView>(R.id.img9)
        var img10 = findViewById<ImageView>(R.id.img10)
        var img11 = findViewById<ImageView>(R.id.img11)
        var img12 = findViewById<ImageView>(R.id.img12)
        var img13 = findViewById<ImageView>(R.id.img13)
        var img14 = findViewById<ImageView>(R.id.img14)
        var img15 = findViewById<ImageView>(R.id.img15)
        var listImg = arrayListOf<ImageView>(
            img1,img2,img3,img4,img5,img6,img7,img8,img9,img10,img11,img12,img13,img14,img15
        )


        GlobalScope.launch(Dispatchers.Main) {
            var list : Deferred<ArrayList<String>> = async(Dispatchers.IO) {
                 var arrayList = arrayListOf<String>()
                 var res =  ApiServices.create().searchName(
                     "fast",
                     "",
                     1.7F,
                     "",
                     "en",
                     "US",
                     "funny",
                     1
                 )
                when (res.isSuccessful){
                    true -> {
                        for(i in 0 until res.body()!!.data.data.size){
                            res.body().let {
                                arrayList.add(res.body()?.data?.data?.get(i)?.image ?: "")
                            }

                            Log.e("retrofit","tai anh thu $i o Thread:  ${Thread.currentThread().name}")

                        }

                    }

                    else -> {}
                }
                arrayList
            }
            handleUrl(list.await())
            handle(listImg)
            Log.e("glide anh","chen anh o thread:  ${Thread.currentThread().name}")

        }
}

     fun handle(listImg: ArrayList<ImageView>){
        for(i in 0 until 15){
            Glide.with(this@MainActivity).load(linkUrl[i]).into(listImg[i])
        }
    }


     fun handleUrl(list : ArrayList<String>){
        for(i in 0 until 15){
            linkUrl.add(BASE_URL+list[i])
            Log.e("halnde url","halnde url thu $i o thread:  ${Thread.currentThread().name}")
        }
    }

    
}